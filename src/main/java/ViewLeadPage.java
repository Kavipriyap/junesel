import com.leafBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations {

	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if (text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		} else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}

	public MyLeadsPage clickonDeleteLead() {
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}

	public String getId() {
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println("Get ID:" + text);
		String id = text.replaceAll("\\D", "");
		System.out.println("id is : " + id);
		return id;
		//return new FindLeadPage();

	}

}
